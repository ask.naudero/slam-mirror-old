cp extract /usr/local/bin
cp pextract /usr/local/bin
chmod +x /usr/local/bin/extract && chmod +x /usr/local/bin/pextract
cp .netrc /root/.netrc
chmod 600 .netrc
chmod +x aria.sh

if [[ -n $CREDS_URL ]]; then
	wget -q $CREDS_URL -O /usr/src/app/bot/credentials.json
fi

if [[ -n $TOKEN_PICKLE_URL ]]; then
	wget -q $TOKEN_PICKLE_URL -O /usr/src/app/bot/token.pickle
fi

if [[ -n $ACCOUNTS_ZIP_URL ]]; then
	wget -q $ACCOUNTS_ZIP_URL -O /usr/src/app/bot/accounts.zip &> /dev/null
	unzip accounts.zip -d /usr/src/app/bot/accounts &> /dev/null
	rm accounts.zip
fi

if [[ -n $ACCOUNTS_REPO_URL ]]; then
	git clone $ACCOUNTS_REPO_URL /usr/src/app/bot/accounts &> /dev/null
fi

python3 update.py && python3 -m bot
