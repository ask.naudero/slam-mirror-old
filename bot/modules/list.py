from telegram import InlineKeyboardMarkup
from telegram.ext import CommandHandler, CallbackQueryHandler
from bot.helper.mirror_utils.upload_utils.gdtot_helper import GDTOT
from bot.helper.mirror_utils.upload_utils.gdriveTools import GoogleDriveHelper
from bot import LOGGER, dispatcher, STOP_DUPLICATE
from bot.helper.telegram_helper.message_utils import sendMessage, editMessage, sendMarkup
from bot.helper.telegram_helper.filters import CustomFilters
from bot.helper.telegram_helper.bot_commands import BotCommands
from bot.helper.telegram_helper import button_build
from bot.helper.ext_utils.bot_utils import is_gdtot_link

def list_buttons(update, context):
    user_id = update.message.from_user.id
    try:
        key = update.message.text.split(" ", maxsplit=1)[1]
    except IndexError:
        return sendMessage('Send a search key along with command', context.bot, update)
    buttons = button_build.ButtonMaker()
    buttons.sbutton("Drive Root", f"types {user_id} root")
    buttons.sbutton("Recursive", f"types {user_id} recu")
    buttons.sbutton("Cancel", f"types {user_id} cancel")
    button = InlineKeyboardMarkup(buttons.build_menu(2))
    sendMarkup('Choose option to list.', context.bot, update, button)

def select_type(update, context):
    query = update.callback_query
    user_id = query.from_user.id
    msg = query.message
    key = msg.reply_to_message.text.split(" ", maxsplit=1)[1]
    data = query.data
    data = data.split(" ")
    if user_id != int(data[1]):
        query.answer(text="Not Yours!", show_alert=True)
    elif data[2] in ["root", "recu"]:
        query.answer()
        buttons = button_build.ButtonMaker()
        buttons.sbutton("Folders", f"types {user_id} folders {data[2]}")
        buttons.sbutton("Files", f"types {user_id} files {data[2]}")
        buttons.sbutton("Both", f"types {user_id} both {data[2]}")
        buttons.sbutton("Cancel", f"types {user_id} cancel")
        button = InlineKeyboardMarkup(buttons.build_menu(2))
        editMessage('Choose option to list.', msg, button)
    elif data[2] in ["files", "folders", "both"]:
        query.answer()
        list_method = data[3]
        item_type = data[2]
        editMessage(f"<b>Searching for <i>{key}</i></b>", msg)
        list_drive(key, msg, list_method, item_type)
    else:
        query.answer()
        editMessage("list has been canceled!", msg)


def list_drive(key, bmsg, list_method, item_type):
    LOGGER.info(f"listing: {key}")
    list_method = list_method == "recu"
    gdrive = GoogleDriveHelper()
    msg, button = gdrive.drive_list(key, isRecursive=list_method, itemType=item_type)
    if button:
        editMessage(msg, bmsg, button)
    else:
        editMessage(f'No result found for <i>{key}</i>', bmsg)


def gdtot_cloner(update, context):
    if update.message.from_user.username:
        uname = f'@{update.message.from_user.username}'
    else:
        uname = f'<a href="tg://user?id={update.message.from_user.id}">{update.message.from_user.first_name}</a>'
    if uname is not None:
        cc = f'\n\n<b>cc: </b>{uname}'
    try:
        search = update.message.text.split(' ',maxsplit=1)[1]
        reply = sendMessage('Extracting please wait ....', context.bot, update)
        if not is_gdtot_link(search):
            editMessage("GDToT link invalied ....", reply, None)
            return
        gdrive = GoogleDriveHelper()
        gdtot = GDTOT(search)
        if STOP_DUPLICATE:
            file_name_ = gdtot.get_filename()
            if file_name_ is not None:
                smsg, button = gdrive.drive_list(file_name_)
                if smsg:
                    msg3 = "File is already available in Drive.\nHere are the search results:"
                    editMessage(msg3, reply, button)
                    return
            else:
                sendMessage('The provided link Invlied ....', context.bot, update)
                return
        LOGGER.info(f"Extracting gdtot link: {search}")
        button = None
        file_name, file_url = gdtot.parse()
        if file_name == 404:
            sendMessage(file_url, context.bot, update)
            return
        if file_url != 404:
            msg, button = gdrive.clone(file_url)
            delete_msg = gdrive.deletefile(file_url)
        if button:
            editMessage(msg + cc, reply, button)
        else:
            editMessage(file_name, reply, button)
    except IndexError:
        sendMessage('Send cmd along with url', context.bot, update)
    except Exception as e:
        LOGGER.info(e)



list_handler = CommandHandler(BotCommands.ListCommand, list_buttons, filters=CustomFilters.authorized_chat | CustomFilters.authorized_user, run_async=True)
list_type_handler = CallbackQueryHandler(select_type, pattern="types", run_async=True)
gdtot_handler = CommandHandler(BotCommands.GDTOTCommand, gdtot_cloner, filters=CustomFilters.authorized_chat | CustomFilters.authorized_user, run_async=True)

dispatcher.add_handler(gdtot_handler)
dispatcher.add_handler(list_handler)
dispatcher.add_handler(list_type_handler)
